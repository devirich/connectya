# ConnectYA

This script will handle all the technical side of getting your OUYA configured and connected to the new OUYA server.

## Description
In order to make full use of your OUYA now that the Razer/OUYA servers have shut down, we've created a new server that you can connect to that hosts 90%+ of all OUYA games that were ever made in addition to dozens of new games.

This script will by default perform an interactive session with you to setup your OUYA to whatever you would like.

## Usage
To use this script, you can directly invoke it over the internet. Let me give you this warning that running unknown code from the internet can be dangerous. Especially on untrusted connections. Even if you audit the source code yourself (which you can do on this site here), if your internet connection is not properly secured, automated servers could replace this script with a malicious one.

Before you run the script, please ensure that you are securely connected to github.com site (no warning symbols around the domain name or such)


## Support
For issues with this script, either raise an issue here or get onto our Discord server and ask your question in #help.

## License
MIT
