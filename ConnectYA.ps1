[cmdletbinding()]
param(
    [ValidateSet(
        "InteractiveSession",

        "GetServer",
        "GetRoot",
        "TestRoot",

        "SetServer",
        "EnableRoot",
        "FirmwareFlash",
        "BypassOobePairing",
        "BypassDrm",

        "CaptureLogcat",

        "ClearServer",
        "RunShell",
        "DisableRoot",
        "SubmitTranscript"
    )]
    $Action = "InteractiveSession",

    $Scriptblock,

    [string]$WorkingDir = ([IO.Path]::GetTempPath()),

    [switch]$WhatIf,
    [switch]$Force,

    $IPAddress,

    $ServerIPAddress = "178.254.13.17"
)
$ScriptParam = @{
    WhatIf    = $WhatIf
    IPAddress = $IPAddress
}

if ($WorkingDir -eq [IO.Path]::GetTempPath()) {
    $WorkingDir = Join-Path -Path $WorkingDir -ChildPath "ConnectYa"
}

# function adb { & $adb_path $args }


function Test-OuyaConnection {
    param(
        $IPAddress
    )

    $connected = $false
    if ($match = [regex]::Match((& $adb_path devices), "\w*\d\w*").Value) {
        "OUYA connected. Continuing..." | Write-Verbose
        $connected = "Connected via USB to device {0}" -f $match
    }
    elseif ($IPAddress) {
        & $adb_path connect "${IPAddress}:5555" | Out-Null
        if ((& $adb_path devices) -match "\d") {
            $connected = $true
        }
    }

    if ($connected) {
        $connected
    }
    else {
        $false
    }
}

function Repair-OuyaConnection {
    do {
        "OH NO, your OUYA isn't recognized. Lets try a few things to get your OUYA connected.
    1. Do you have it powered on?
    2. Do you have it connected to your computer via the microUSB port?
    Press any key when you want to test if it is connected now (or press q to quit)"
        $key = [console]::ReadKey($true)
        if ($key.KeyChar -eq "q") { return }
    } until (Test-OuyaConnection)
}
function Get-OuyaStatus {
    "Testing OUYA connection status."
    $result = Test-OuyaConnection
    "Result: $result"
    if (-not $result) { Repair-OuyaConnection }
}

function Get-OuyaServer {
    [string[]]$res = @()

    $res = & $adb_path shell ls $config_path
    if ($res -match "No such file") {
        Write-Host -ForegroundColor Yellow "No ouya server config at $config_path"
    }
    else {
        $res = & $adb_path shell cat $config_path
        $res = $res -split "`r?`n"
        Write-Verbose "Contents of $config_path"
        $res | Where-Object { $_ } | Write-Verbose
        if ($out = $res -match "^\s*OUYA_SERVER_URL=" | Select-Object -Last 1) {
            Write-Host -ForegroundColor Green "Ouya server in config is set to: $($out -replace '.*=')"
        }
        else {
            Write-Host -ForegroundColor Yellow "No OUYA server set in $config_path"
        }
    }
    $res = & $adb_path shell ls $hosts_path
    if ($res -match "No such file") {
        Write-Host -ForegroundColor Red "No hosts at $hosts_path"
    }
    else {
        $res = & $adb_path shell cat $hosts_path
        $res = $res -split "`r?`n"
        Write-Verbose "Contents of $hosts_path"
        $res | Where-Object { $_ } | Write-Verbose
        if ($out = $res -match " devs.ouya.tv" | Select-Object -Last 1) {
            Write-Host -ForegroundColor Green "Ouya server is resolving to (via hosts): $($out -replace ' .*')"
        }
        else {
            Write-Host -ForegroundColor Yellow "No OUYA server set in $hosts_path"
        }
        Write-Host "Testing hostname mapping"
        $res = & $adb_path shell ping -c1 devs.ouya.tv
        $res = $res -split "`r?`n"
        if ($out = $res[0] -match $ServerIPAddress) {
            Write-Host -ForegroundColor Green "Ouya server is searching for devs.ouya.tv at IP: $($matches[0])"
        }
        elseif ($out = $res[0] -match "(\d+\.){3}\d+") {
            Write-Warning "Ouya server is searching for devs.ouya.tv at IP: $($matches[0])"
        }
        else {
            Write-Host -ForegroundColor Red "Unexpected result when searching for devs.ouya.tv: $($res[0])"
        }
    }
}

function Set-OuyaServer {
    [cmdletbinding()]
    param(
        $Uri = "ouya.cweiske.de"
    )
    if ($Uri -notmatch "^http") {
        $Uri = "http://$uri"
    }
    "setting OUYA to $uri..."


    $path = $config_path
    $text = @"
    OUYA_SERVER_URL=$uri
    OUYA_STATUS_SERVER_URL=$uri/api/v1/status
"@ -replace "(?m)^\s*|`r" -replace "`n", '\n'

    Write-Verbose "echo '$text' > $path"
    & $adb_path shell "echo '$text' > $path"

    $text_result = ((& $adb_path shell cat $path) | Where-Object { $_ }) -join "\n"
    if ($text_result.length -ne $text.Length) {
        Write-Host -ForegroundColor Red "$path did not get set right."
        Write-Host -ForegroundColor Red "Expected:`n$text"
        Write-Host -ForegroundColor Red "Actual:`n$text_result"
        Write-Host -ForegroundColor Yellow "You likely need to flash your OUYA to clean up the system"
    }
    else {
        Write-Host -ForegroundColor Green "OuyaConfig at $path is good to go."
    }


    # TODO: Fix this. Doesn't set anything!
    $path = $hosts_path
    $text = @"
        127.0.0.1 localhost
        $ServerIPAddress devs.ouya.tv
        $ServerIPAddress status.ouya.tv
        $ServerIPAddress ouya-updates.s3.amazonaws.com
"@ -replace "(?m)^\s*|`r"

    @(
        "mount -o rw,remount -t ext4 /dev/block/platform/sdhci-tegra.3/by-name/APP"
        "rm $path"
        "touch $path"
        $text -split "`n" | ForEach-Object { "echo `"$_`" >> $path" }
        "mount -o ro,remount -t ext4 /dev/block/platform/sdhci-tegra.3/by-name/APP"
    ) | ForEach-Object {
        Write-Verbose "& $adb_path shell su -c '$_'"
        if ((Get-OuyaRootStatus -ReturnResult) -in "stock", "other") { & $adb_path shell su -c $_ }
        else { & $adb_path shell su -c "'$_'" }
    }

    $text_result = ((& $adb_path shell cat $path) | Where-Object { $_ }) -join "`n"
    if ($text_result.length -ne $text.Length) {
        Write-Host -ForegroundColor Red "$path did not get set right."
        Write-Host -ForegroundColor Red "Expected:`n$text"
        Write-Host -ForegroundColor Red "Actual:`n$text_result"
        Write-Host -ForegroundColor Yellow "You likely need to flash your OUYA to clean up the system"
    }
    else {
        Write-Host -ForegroundColor Green "Hosts at $path is good to go."
    }
}

function Clear-OuyaServer {
    [cmdletbinding()]
    param(
    )
    "setting OUYA to no server..."


    $path = $config_path


    Write-Verbose "rm $path"
    & $adb_path shell "rm $path"

    $out = & $adb_path shell " if (test -f /sdcard/ouya_config.properties);then echo 'found';else echo 'missing';fi"
    if ($out -eq "found") {
        $text_result = ((& $adb_path shell cat $path) | Where-Object { $_ }) -join "`n"
        Write-Host -ForegroundColor Red "$path did not get cleared right."
        Write-Host -ForegroundColor Red "Expected: no file"
        Write-Host -ForegroundColor Red "Actual:`n$text_result"
        Write-Host -ForegroundColor Yellow "You likely need to flash your OUYA to clean up the system"
    }
    elseif ($out -eq "missing") {
        Write-Host -ForegroundColor Green "OuyaConfig at $path is cleared."
    }
    else {
        Write-Host -ForegroundColor Red "Unable to determine status of file at expected path: $path"
    }


    $path = $hosts_path
    $text = @"
        127.0.0.1 localhost
"@ -replace "(?m)^\s*|`r"

    @(
        "mount -o rw,remount -t ext4 /dev/block/platform/sdhci-tegra.3/by-name/APP"
        "rm $path"
        "touch $path"
        $text -split "`n" | ForEach-Object { "echo `"$_`" >> $path" }
        "mount -o ro,remount -t ext4 /dev/block/platform/sdhci-tegra.3/by-name/APP"
    ) | ForEach-Object {
        Write-Verbose "& $adb_path shell su -c '$_'"
        if ((Get-OuyaRootStatus -ReturnResult) -in "stock", "other") { & $adb_path shell su -c $_ }
        else { & $adb_path shell su -c "'$_'" }
    }


    $text_result = ((& $adb_path shell cat $path) | Where-Object { $_ }) -join "`n"
    if ($text_result.length -ne $text.Length) {
        Write-Host -ForegroundColor Red "$path did not get set right."
        Write-Host -ForegroundColor Red "Expected:`n$text"
        Write-Host -ForegroundColor Red "Actual:`n$text_result"
        Write-Host -ForegroundColor Yellow "You likely need to flash your OUYA to clean up the system"
    }
    else {
        Write-Host -ForegroundColor Green "Hosts at $path is good to go."
    }
}

function Get-OuyaRootStatus {
    param(
        [switch]$ReturnResult
    )
    if (-not $ReturnResult) {
        Test-OuyaRoot
        "Checking su version..."
    }
    $path = "/system/xbin/su"
    $hosts = "/etc/hosts"
    $res = & $adb_path shell md5sum $path
    $res = $res -split "`r?`n"
    if ($res[0] -eq "450f57ac1700be5387bc76d01f59dd40  $path") {
        if (-not $ReturnResult) {
            Write-Host -ForegroundColor Green "Ouya Root has the stock SU binary"
            Write-Host -ForegroundColor Red "The OUYA is NOT rooted"
        }
        else {
            "stock"
        }
    }
    elseif ($res[0] -eq "8e9262aeae3382849acf3da2f82663d5  $path") {
        if (-not $ReturnResult) {
            Write-Host -ForegroundColor Green "Ouya Root has the main community SU binary"
        }
        else {
            "community"
        }
    }
    elseif ($res[0] -eq "md5sum: can't open '$path': No such file or directory") {
        if (-not $ReturnResult) {
            Write-Host -ForegroundColor Red "Ouya has NO su binary!"
        }
        else {
            "none"
        }
    }
    else {
        if (-not $ReturnResult) {
            Write-Warning "Ouya Root has a non-stock SU binary"
        }
        else {
            "other"
        }
    }

    @(
        "ls -l $path"
        "rm $hosts"
        "touch $hosts"
        $text -split "`n" | ForEach-Object { "echo `"$_`" >> $hosts" }
        "mount -o ro,remount -t ext4 /dev/block/platform/sdhci-tegra.3/by-name/APP"
    ) | ForEach-Object {
        # Write-Verbose "INACTIVE!! & $adb_path shell su -c '$_'"
        # & $adb_path shell su -c "'$_'"
    }
}

function Test-OuyaRoot {
    Write-Host "Testing root status now..."
    @(
        "whoami"
    ) | ForEach-Object {
        Write-Verbose "& $adb_path shell su -c '$_'"
        $res = & $adb_path shell su -c "'$_'"
        $res = $res -split "`r?`n"
        if ($res[0] -eq "root") {
            Write-Host -ForegroundColor Green "Root is working"
        }
        elseif ($res[0] -eq "shell") {
            Write-Host -ForegroundColor Red "Root is NOT working. You need to factory reset (and probably best to reflash the firmware with latest too)."
        }
        else {
            Write-Host -ForegroundColor Red "Unexpected error checking root"
        }
    }
}

function Enable-OuyaRoot {
    Write-Host "Rooting OUYA now..."
    $su = "/system/xbin/su"
    $su_temp = "/sdcard/su"
    $su_community = Get-OuyaSiteItem -SiteName su_community
    & $adb_path push $su_community.Fullname $su_temp
    $res = & $adb_path shell md5sum $su_temp
    $res = $res -split "`r?`n"
    if ($res[0] -eq "8e9262aeae3382849acf3da2f82663d5  $su_temp") {
        Write-Host -ForegroundColor Green "Verified good binary copy 8e92"
    }
    "testing"
    "Installing to $su now"


    @(
        "mount -o rw,remount -t ext4 /dev/block/platform/sdhci-tegra.3/by-name/APP"
        "cp $su_temp ${su}_temp"
        "chmod 6755 ${su}_temp"
        "mv ${su}_temp $su"
        "mount -o ro,remount -t ext4 /dev/block/platform/sdhci-tegra.3/by-name/APP"
    ) | ForEach-Object {
        Write-Verbose "& $adb_path shell su -c '$_'"
        if ((Get-OuyaRootStatus -ReturnResult) -in "stock", "other") { & $adb_path shell su -c $_ }
        else { & $adb_path shell su -c "'$_'" }
    }

    Get-OuyaRootStatus
}

function Disable-OuyaRoot {
    Write-Host "Unrooting OUYA now..."
    $su = "/system/xbin/su"
    $su_temp = "/sdcard/su"
    $su_stock = Get-OuyaSiteItem -SiteName su_stock
    & $adb_path push $su_stock.Fullname $su_temp
    $res = & $adb_path shell md5sum $su_temp
    $res = $res -split "`r?`n"
    if ($res[0] -eq "450f57ac1700be5387bc76d01f59dd40  $su_temp") {
        Write-Host -ForegroundColor Green "Verified good binary copy 450f"
    }
    "testing"
    "Installing to $su now"

    @(
        "mount -o rw,remount -t ext4 /dev/block/platform/sdhci-tegra.3/by-name/APP"
        "cp $su_temp ${su}_temp"
        "chmod 6755 ${su}_temp"
        "mv ${su}_temp $su"
        "mount -o ro,remount -t ext4 /dev/block/platform/sdhci-tegra.3/by-name/APP"
    ) | ForEach-Object {
        Write-Verbose "& $adb_path shell su -c '$_'"
        if ((Get-OuyaRootStatus -ReturnResult) -in "stock", "other") { & $adb_path shell su -c $_ }
        else { & $adb_path shell su -c "'$_'" }
    }

    Get-OuyaRootStatus
}

function Unblock-OuyaDrm {
    if ((Get-OuyaRootStatus -ReturnResult) -ne "community") {
        Write-Warning "The OUYA does not have the community su binary installed currently. Are you sure you want to continue?"
        Write-Warning "You should say no/n and root the OUYA first before continuing unless you know what you're doing."
        $response = Read-Host "Continue? (y/n)"
        if ($response -notin "y", "yes") {
            Write-Warning "Exiting unblocking command"
            return
        }
    }



    Write-Host "Adding apks now..."
    @(
        "su_apk"
        "xposed_apk"
        "plainPurchases_apk"
    ) | ForEach-Object {
        "Downloading $_ now." | Write-Host
        $apk = Get-OuyaSiteItem $_
        "Installing $($apk.Fullname) now." | Write-Host
        & $adb_path install $apk.Fullname
    }

    Write-Host "Go to the 'Make' menu and launch the Xposed app"
    Pause
    Write-Host "Enter the 'Framework' menu and after reading it, click OK to the big prompt that says, 'Be careful!'"
    & $adb_path shell input tap 450 320
    Start-Sleep 1
    & $adb_path shell input tap 959 815
    Start-Sleep 1
    Write-Host "Under the 'Framework' section, click 'Install/Update'"
    & $adb_path shell input tap 900 400
    Start-Sleep 1
    Write-Host "Click 'Allow' on the request for Superuser access"
    Start-Sleep 4
    & $adb_path shell input tap 1150 900
    Start-Sleep 1
    & $adb_path shell input tap 1200 755
    Write-Host "Click 'OK' to reboot."
    & $adb_path reboot
    Write-Host "Wait for OUYA to reboot then launch Xposed from the Make menu again"
    Pause
    Write-Host "Enter the 'Modules' section"
    & $adb_path shell input tap 300 460
    Start-Sleep 1
    Write-Host "Enable the checkbox for the Plain Purchases module (here, I'll attempt to check it for you)"
    & $adb_path shell input tap 1754 220
    Start-Sleep 1
    Write-Host "Reboot OUYA"
    & $adb_path reboot
    Start-Sleep 1
    Write-Host -ForegroundColor Green "You're all done!"
    Pause
}

function Invoke-OuyaFirmwareFlash {
    [CmdletBinding()]
    param(
        $LocalDir = $ENV:TEMP
    )

    Write-Host -ForegroundColor Red "THIS PROCESS DELETES ALL DATA ON YOUR OUYA!"
    Write-Host -ForegroundColor Red "You will need: A USB keyboard to hook up to your OUYA."
    if ($Force) {}
    elseif ($answer = Read-Host "Continue (y/n)") {
        if ($answer -notin "y", "yes") {
            return "You didn't answer y/yes, exiting."
        }
    }
    else { return "You didn't answer, exiting." }

    Write-Host -ForegroundColor Red "Continuing! Download is over 120MB, this will take a few minutes..."
    $firmware = Get-OuyaSiteItem firmware

    Write-Host "Plug in the USB keyboard into the OUYA now"
    Pause
    Write-Host "Rebooting OUYA to recovery now..."
    & $adb_path reboot recovery
    Start-Sleep 8
    Write-Host "You should see the word 'OUYA' followed by a red exclamation point."
    Write-Host "Press the 'Home' key on your keyboard to get to the recovery menu (its normally near Insert, Delete, Page Up/Down)"
    Pause
    Write-Host "Use the up and down arrow keys on your keyboard to select the menu option 'apply update from ADB' and press Enter"
    Write-Host "It should now say 'Now send the package you want to apply to the device with 'adb sideload filename'"
    Pause
    Write-Host "Please wait a few minutes while the firmware gets flashed"
    & $adb_path sideload $firmware.Fullname
    Write-Host "Sideload sent. OUYA should now be updating. Once it is complete, you will see the recovery menu back at the top."
    Pause
    Write-Host "Ok, done flashing. Now use the up/down arrow keys to select the menu item 'wipe data/factory with the enter key"
    Write-Host "Then select the 'Yes - Wipe all data' option and press enter"
    Pause
    Write-Host -ForegroundColor Green "Ok, all done! Your OUYA has been reflashed and reset to factory defaults"
    Write-Host -ForegroundColor Yellow "If you want to use the OUYA community server, be sure to enable the community server BEFORE DOING ANYTHING (like pairing the controller)"
}

function Suspend-OuyaOobePairing {
    & $adb_path shell am start -n tv.ouya.console/.launcher.home.HomeActivity
}

function Get-OuyaSiteItem {
    param(
        $SiteName
    )

    if ($IsLinux) { $platform = "linux" }
    elseif ($IsMacOS) { $platform = "darwin" }
    else { $platform = "windows" }

    $sites = @{
        platform_tools     = [uri]"https://dl.google.com/android/repository/platform-tools-latest-$platform.zip"
        firmware           = [uri]"https://archive.org/download/ouya_firmwarepackages/RC-OUYA-1.2.1427-r1_ota.zip"
        su_community       = [uri]"https://statics.ouya.world/su_community"
        su_stock           = [uri]"https://statics.ouya.world/su_stock"
        cweiske            = [uri]"http://ouya.cweiske.de"
        su_apk             = [uri]"https://statics.ouya.world/ia/koushikdutta-ouya-superuser/com.koushikdutta.superuser-v1.0.3.0.apk"
        xposed_apk         = [uri]"https://ouya.cweiske.de/unlocking-tools/de.robv.android.xposed.installer-2.6.1.apk"
        plainPurchases_apk = [uri]"https://ouya.cweiske.de/unlocking-tools/xposed-ouya-plain-purchases-1.1.0.apk"
    }

    if ($SiteName -notin $sites.keys) {
        throw "No site named $SiteName found. Exiting."
    }

    $downloadPath = Join-Path $WorkingDir $sites[$SiteName].segments[-1]
    if (Test-Path $downloadPath) {
        return Get-ChildItem $downloadPath
    }

    $attempt = 0
    $pending = $true
    do {
        $attempt++
        try {
            Invoke-WebRequest -OutFile $downloadPath -Uri $sites[$SiteName] -ea stop
            Get-ChildItem $downloadPath
            $pending = $false
        }
        catch {
            if ($attempt -gt 3) {
                $pending = $false
                "Unable to download $SiteName from site, printing details below. Please figure out why and then run this command again. Attempted to download: {0}" -f $sites[$SiteName] | Write-Warning
                $_ | Write-Error
                return "failed"
            }
            else {
                "Failed to download $SiteName, trying again in a moment..." | Write-Warning
                Start-Sleep 1
            }
        }
    } while ($pending)
    Write-Progress -Activity foo -Completed
}

function Submit-Transcript {
    param(
        $Duration = 30,
        $Language = "text"

    )
    $log = Get-Content $transcript_path
    $log = ($log -replace "(username|runas user|machine:).*" -replace "\w:\\users\\\w+", "<USERPROFILE>" -join "`n")
    if ($log.Length -gt 1mb) {
        $log = $log.Substring(($log.Length - 1mb), 1mb)
    }
    $result = Invoke-RestMethod "https://www.pastery.net/api/paste/?duration=$Duration&language=$Language&title=$title" -Method Post -Body $log
    "Send this link to whoever is helping you (link is only good for $Duration minutes): {0}" -f $result.url | Write-Host -ForegroundColor Green -BackgroundColor Black
    if (-not $IsLinux -and -not $IsMacOS) {
        $result.url | Set-Clipboard
        "URL copied to clipboard for you :D" | Write-Host
    }
    Pause
}

function Invoke-OuyaShell {
    "Type 'Exit' to return..."
    & $adb_path shell
}

function Restart-Ouya {
    & $adb_path reboot
    Start-Sleep 30
}

function Get-OuyaLogcat {
    "Press Enter now to begin listening for log data" | Write-Host
    "Press Q to stop listening for log data" | Write-Host
    Pause
    # $results = & $adb_path logcat -d
    & $adb_path pull "/sdcard/ouya_config.properties" "$WorkingDir\ouya_config.properties" | Out-Null
    "`nDEBUG=1" | Out-File "$WorkingDir\ouya_config.properties" -Append -Encoding ascii
    & $adb_path push "$WorkingDir\ouya_config.properties" "/sdcard/ouya_config.properties" | Out-Null
    (Get-Content "$WorkingDir\ouya_config.properties") | Where-Object { $_ -notmatch "DEBUG=" } | Where-Object { $_ } |
    Out-File "$WorkingDir\ouya_config.properties" -Encoding ascii

    & $adb_path shell rm /sdcard/logcat.log | Out-Null
    & $adb_path logcat -c
    $logcat = Start-Job -ScriptBlock { & $using:adb_path logcat -f /sdcard/logcat.log }
    $output = Start-Job -ScriptBlock {
        & $using:adb_path shell tail -f /sdcard/logcat.log | Where-Object { $_ }
    }
    "Monitoring now" | Write-Host -ForegroundColor Green
    [System.Collections.Generic.List[string]]$final = @()
    do {
        if ($output.HasMoreData) {
            $results = Receive-Job $output
            $results.foreach{ $final.Add($_) }
            $results
        }
        Start-Sleep -Milliseconds 100
    } until ([console]::KeyAvailable -and [console]::ReadKey($true).key -eq "q")
    Write-Progress -Activity "Cleaning up..."
    $logcat, $output | Stop-Job -PassThru | Remove-Job

    & $adb_path pull "/sdcard/ouya_config.properties" "$WorkingDir\ouya_config.properties" | Out-Null
    (Get-Content "$WorkingDir\ouya_config.properties") | Where-Object { $_ -notmatch "DEBUG=" } | Where-Object { $_ } |
    Out-File "$WorkingDir\ouya_config.properties" -Encoding ascii
    & $adb_path push "$WorkingDir\ouya_config.properties" "/sdcard/ouya_config.properties" | Out-Null

    Write-Progress -Activity "Cleaning up..." -Completed
}


if ((Get-History).count -lt 10) { Clear-Host }
# Download ADB
# Check the OUYA status
#

$title = "ConnectYA v0.3.0"
$title

if ($IsLinux) {
    $WorkingDir = "~/connectya"
}

if (-not (Test-Path $WorkingDir)) { mkdir $WorkingDir | Out-Null }

try {
    & adb version
    if ((& adb version | Select-Object -First 1 | ForEach-Object { $_ -replace "[^\d.]" -as [version] }) -lt [version]"1.0.31") {
        Write-Warning "Listed ADB is too old. Getting a newer version instead"
        throw "Old ADB found"
    }
    $adb_path = "adb"
}
catch {
    $adb_path = Join-Path $WorkingDir -ChildPath platform-tools | Join-Path -ChildPath adb
    if (-not $IsLinux -and -not $IsMacOS) { $adb_path += ".exe" }


    if (-not (Test-Path $adb_path)) {
        $platformTools = Get-OuyaSiteItem platform_tools
        Expand-Archive -Path $platformTools.Fullname -DestinationPath "$WorkingDir$([io.path]::DirectorySeparatorChar)" -Force
    }
}

$transcript_path = Join-Path -Path $WorkingDir -Child transcripts | Join-Path -ChildPath "$title - $(Get-Date -f 'yyyy-MM-dd hh_mm_ss').txt"
try { Stop-Transcript -ea stop | Out-Null } catch {}
try { Start-Transcript $transcript_path -ea stop | Out-Null } catch {}
$hosts_path = "/etc/hosts"
$config_path = "/sdcard/ouya_config.properties"




if (& $adb_path version) { "Found command 'adb' successfully (via: $adb_path)." }
else { "Could not access adb. Check that the platform_tools folder exists in $workingdir and contains adb.exe" }

$menu = ([ordered]@{
        "Get-OuyaServer"           = "Get current OUYA server"
        "Set-OuyaServer"           = "Set OUYA to use community server"
        "Clear-OuyaServer"         = "Clear OUYA community server back to stock settings"
        "Test-OuyaRoot"            = "Check to see if OUYA root binary (su) is usable"
        "Get-OuyaRootStatus"       = "Check to see if OUYA is rooted"
        "Enable-OuyaRoot"          = "Root the OUYA"
        "Unblock-OuyaDrm"          = "Unlock OUYA games (bypass DRM for most every game)"
        "Invoke-OuyaFirmwareFlash" = "Flash the OUYA firmware (defaults to latest official firmware version)"
        "Invoke-OuyaShell"         = "Enter OUYA ADB shell"
        "Restart-Ouya"             = "Reboot the OUYA"
        "Get-OuyaLogcat"           = "Capture logcat data"
        "Submit-Transcript"        = "Submit log data to the internet so you can share"
        "Disable-OuyaRoot"         = "Unroot the OUYA (restore su binary to original)"
        "Suspend-OuyaOobePairing"  = "Temporarily bypass OOBE controller pairing until reboot"
    }).GetEnumerator() | ForEach-Object { $_ }

switch ($Action) {
    "InteractiveSession" {
        $continue = $true
        while ($continue) {
            Get-OuyaStatus
            Get-OuyaServer
            "Here are your options:"
            $i = 1
            $menu | ForEach-Object {
                "    $i. {0}" -f $_.Value
                $i++
            }
            "    $i/Q. Quit"
            do {
                $response = Read-Host "Please type which option you'd like to do"
            } until ($response -in 1..($menu.Count + 1) -or $response -eq 'q')
            if ($response -eq $i -or $response -eq 'q') { $continue = $false }
            else {
                Write-Host "`n`n`n"
                "execute: {0}" -f $menu[$response - 1].Name
                &  $menu[$response - 1].Name
            }
        }
    }

    "SetServer" { Set-OuyaServer }
    "EnableRoot" { Enable-OuyaRoot }
    "BypassDrm" { Unblock-OuyaDrm }
    "FirmwareFlash" { Invoke-OuyaFirmwareFlash }
    "GetServer" { Get-OuyaServer }
    "ClearServer" { Clear-OuyaServer }
    "TestRoot" { Test-OuyaRoot }
    "GetRoot" { Get-OuyaRootStatus }
    "RunShell" { Invoke-OuyaShell }
    "Reboot" { Restart-Ouya }
    "DisableRoot" { Disable-OuyaRoot }
    "BypassOobePairing" { Suspend-OuyaOobePairing }
    "CaptureLogcat" { Get-OuyaLogcat }
    "SubmitTranscript" { Submit-Transcript }
    default { "No option selected, exiting." }
}
"Exiting"
try { Stop-Transcript -ea stop | Out-Null } catch {}
